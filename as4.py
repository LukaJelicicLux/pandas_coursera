import pandas as pd
import numpy as np
from scipy.stats import ttest_ind

states = {'OH': 'Ohio', 'KY': 'Kentucky', 'AS': 'American Samoa', 'NV': 'Nevada', 'WY': 'Wyoming', 'NA': 'National', 'AL': 'Alabama', \
          'MD': 'Maryland', 'AK': 'Alaska', 'UT': 'Utah', 'OR': 'Oregon', 'MT': 'Montana', 'IL': 'Illinois', 'TN': 'Tennessee', 'DC': 'District of Columbia', \
          'VT': 'Vermont', 'ID': 'Idaho', 'AR': 'Arkansas', 'ME': 'Maine', 'WA': 'Washington', 'HI': 'Hawaii', 'WI': 'Wisconsin', 'MI': 'Michigan', 'IN': 'Indiana',\
          'NJ': 'New Jersey', 'AZ': 'Arizona', 'GU': 'Guam', 'MS': 'Mississippi', 'PR': 'Puerto Rico', 'NC': 'North Carolina', 'TX': 'Texas', 'SD': 'South Dakota', \
          'MP': 'Northern Mariana Islands', 'IA': 'Iowa', 'MO': 'Missouri', 'CT': 'Connecticut', 'WV': 'West Virginia', 'SC': 'South Carolina', 'LA': 'Louisiana', \
          'KS': 'Kansas', 'NY': 'New York', 'NE': 'Nebraska', 'OK': 'Oklahoma', 'FL': 'Florida', 'CA': 'California', 'CO': 'Colorado', 'PA': 'Pennsylvania', 'DE': \
          'Delaware', 'NM': 'New Mexico', 'RI': 'Rhode Island', 'MN': 'Minnesota', 'VI': 'Virgin Islands', 'NH': 'New Hampshire',\
          'MA': 'Massachusetts', 'GA': 'Georgia', 'ND': 'North Dakota', 'VA': 'Virginia'}

# Zillow research data file 

zil = pd.read_csv('City_Zhvi_AllHomes.csv').drop(['Metro','CountyName','RegionID','SizeRank'], axis=1)
zil['State'] = zil['State'].map(states)




# GDP over time


with pd.ExcelFile('gdplev.xls') as xls:
    gdp = pd.read_excel(xls, skiprows=5)

gdp = gdp.drop([0,1], axis='index').drop(['Unnamed: 3','Unnamed: 7'], axis=1)
gdp = gdp[['Unnamed: 4', 'GDP in billions of current dollars.1']][221-9:]



def get_list_of_university_towns():
    
    un = []
    with open('university_towns.txt') as ut:
        for line in ut:
            if '[edit]' in line:
                state = line.strip().split('[edit]')[0]
                continue
            else:
                town = line.strip().split(' (')[0]
                un.append([state, town])
            
    return pd.DataFrame(un, columns=["State", "RegionName"])





def get_recession_start():
    for i in range(len(gdp)):
        if (gdp.iloc[i-2][1] > gdp.iloc[i-1][1]) and (gdp.iloc[i-1][1] > gdp.iloc[i][1]):
            return gdp.iloc[i-2][0]



def get_recession_end():
 
    def get_recession_index():
        for i in range(len(gdp)):
            if (gdp.iloc[i-2][1] > gdp.iloc[i-1][1]) and (gdp.iloc[i-1][1] > gdp.iloc[i][1]):
                return i-2
            
    g = gdp[get_recession_index():]   
    for i in range(len(g)):
        if (g.iloc[i-2][1] < g.iloc[i-1][1]) and (g.iloc[i-1][1] < g.iloc[i][1]):
            return g.iloc[i][0]





def get_recession_bottom():
    g  = gdp.copy()
    g.set_index("Unnamed: 4", inplace=True)
    g = g[g.index.get_loc(get_recession_start()): g.index.get_loc(get_recession_end())]
    return g['GDP in billions of current dollars.1'].idxmin()
    



def run_ttest():
    start = '2008q3'
    bottom = '2009q2'
    def convert_housing_data_to_quarters():
        quars = ['q1','q2','q3','q4']
        zil.set_index(['State','RegionName'], inplace=True)
        to_drop = zil.columns[:45]
        zil.drop(to_drop, axis=1, inplace=True)
        qs = [list(zil.columns)[x:x+3] for x in range(0, len(list(zil.columns)), 3)]
        cols = []
        for q in qs:
            r = q[0].split('-')
            if r[1] == '01':
                cols.append(str(r[0] + quars[0]))
            if r[1] == '04':
                cols.append(str(r[0] + quars[1]))
            if r[1] == '07':
                cols.append(str(r[0] + quars[2]))
            if r[1] == '10':
                cols.append(str(r[0] + quars[3]))
        for col, q in zip(cols, qs):
            zil[col] = zil[q].mean(axis=1)
        data = zil[cols]
        return data

    data = (convert_housing_data_to_quarters().loc[:, start:bottom]).reset_index()

    data['changes'] = data.apply(lambda row:(row[start] - row[bottom])/row[start] , axis=1)

    get_towns = set(get_list_of_university_towns()['RegionName'])
    data['is_u_town'] = data.apply(lambda row:  True if row['RegionName'] in get_towns else False, axis=1)

    non_u, u = data[data['is_u_town']==False].loc[:,'changes'].dropna(), data[data['is_u_town']==True].loc[:,'changes'].dropna()
    mean = 'non-university town' if u.mean() > non_u.mean() else 'university town'
    p = ttest_ind(non_u, u, nan_policy='omit')[1]
    return (True if p < 0.01 else False, p, mean)

#print(run_ttest())
"""0.00036641601595078404
# test output type (different, p, better)
def test_q6():
    q6 = run_ttest()
    different, p, better = q6

    res = 'Type test: '
    res += ['Failed\n','Passed\n'][type(q6) == tuple]

    res += 'Test "different" type: '
    res += ['Failed\n','Passed\n'][type(different) == bool or type(different) == np.bool_]

    res += 'Test "p" type: '
    res += ['Failed\n','Passed\n'][type(p) == np.float64]

    res +='Test "better" type: '
    res += ['Failed\n','Passed\n'][type(better) == str]
    if type(better) != str:
        res +='"better" should be a string with value "university town" or  "non-university town"'
        return res
    res += 'Test "different" spelling: '
    res += ['Failed\n','Passed\n'][better in ["university town", "non-university town"]]
    return res
print(test_q6())
"""
